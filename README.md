![](./firefox.jpg)

Firefox Multiple Profiles and Versions
======================================

[This is a personal proyect]

Run several Firefox Browsers at the same time.
This allows you to run multiple versions and multiple profiles.

A good example is to be able to run Firefox with the company profile and another with the personal profile.

![preview](./preview.png)

Launchers will be in a folder called `Launchers`.

### Video Tutorial

https://www.youtube.com/watch?v=AaGApdRlU54

### Requirments

##### 1. Firefox Versions

Download the version you need and placed in a folder called  `Versions`

Example:

```bash
    .
    └── Versions
        ├── aurora
        ├── beta
        ├── firefox
        └── nightly
```

Links:

https://www.mozilla.org/en-US/firefox/channel/#firefox

https://nightly.mozilla.org/

##### 2. Create folders for your profiles inside `Profiles` folder or place your
existing profiles inside this folder

Example:

```bash
    .
    └─── Profiles
      ├── profile-name-1
      ├── profile-name-2
      └── profile-name-3
```

**Final Folders estructure**

```
  .
  ├── Profiles
      ├── profile-name-1
      ├── profile-name-2
      └── profile-name-3
  └── Versions
      ├── aurora
      ├── beta
      ├── firefox
      └── nightly
```

##### 3. run the script `firefox-create`

```bash
    ./firefox-create.sh
```
