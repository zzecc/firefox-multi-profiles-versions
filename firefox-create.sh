#!/bin/bash

SCRIPT_PATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )";

# Reset color
RS="\e[0m"
# Basic Colors
BLACK="\e[0;30m"
RED="\e[0;31m"
GREEN="\e[0;32m"
YELLOW="\e[0;33m"
BLUE="\e[0;34m"
PURPLE="\e[0;35m"
CYAN="\e[0;36m"
WHITE="\e[0;37m"

mkdir -p Profiles
mkdir -p Versions

PROFILE=( $(find $SCRIPT_PATH/Profiles -maxdepth 1 -type d -printf '%P\n') )
VERSION=( $(find $SCRIPT_PATH/Versions -maxdepth 1 -type d -printf '%P\n') )


if [ -z "$VERSION" ]
then
echo -e  "\n    - Folder 'Versions' is empty."$RED" Download "$YELLOW"Firefox" $RS
exit 1
fi

if [ -z "$VERSION" ]
then
echo -e  "\n    - No profiles."$RED" Create at least one profile with empty folder" $RS
exit 1
fi

FOLDER_LAUNCHERS='Launchers'

[ -d $SCRIPT_PATH/$FOLDER_LAUNCHERS ] || mkdir $SCRIPT_PATH/$FOLDER_LAUNCHERS

for ix in ${!PROFILE[*]}; do
for ax in ${!VERSION[*]}; do
FILENAME="${PROFILE[$ix]}"-"${VERSION[$ax]}"

echo '#!/bin/bash
VERSION='"${VERSION[$ax]}"'
PROFILE='"${PROFILE[$ix]}"'

SOURCE="${BASH_SOURCE[0]}"

while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
SOURCE="$(readlink "$SOURCE")"
[[ $SOURCE != /* ]] && SOURCE="'"${SCRIPT_PATH}"'/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done

DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
'"${SCRIPT_PATH}"'/Versions/'"${VERSION[$ax]}"'/firefox -profile '"${SCRIPT_PATH}"'/Profiles/'"${PROFILE[$ix]}"' --no-remote' > $FOLDER_LAUNCHERS/$FILENAME

chmod +x $FOLDER_LAUNCHERS/$FILENAME
echo "Launcher created - $FILENAME"
done
done

unset PROFILE
unset VERSION

# ##
# | FIle:               firefox-create.sh
# | Date of creation:   07/Ago/2014
# |
# | This Script Create a launcher for multiple profiles and versions of Firefox
# |
# | @author             Miguel D. Quintero
# | @version            2.0.1
# | @link
# |
# | Revision
# |                     2.0.1 (07/dic/2015)   - Miguel D. Quintero
# |                     2.0.1 (28/Sep/2019)   - Miguel D. Quintero
# ##
